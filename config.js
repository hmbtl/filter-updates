module.exports = {
    "en-node-logger": {
        filters: [
            {
                regexFilter: [{
                    field: "message",
                    pattern: "/asfsaf/",
                    action: "allow"
                },
                {
                    field: "type",
                    pattern: "/info/",
                    action: "deny"
                }]
            },
            {
                rateLimiter: {
                    numberOfLogs: 1000,//logs
                    duraion: 100,//ms
                    drop: true // logger.warn("limited is reached") 
                }
            },
            {
                regexFilter: {
                    field: "bla",
                    pattern: "/asfsaf/",
                    action: 1
                }
            }]
    }
}
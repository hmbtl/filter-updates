import { createLogger, format, transports, Container, loggers, Logger } from "winston";
import { throttle} from "./format"
import { LOGGER_TYPE } from "./enum"


const limiter = throttle();// 10 logs per second

export interface ThrottleOptions {
    numberOfLogs: number,
    duration: number
}

let alignColorsAndTime = format.combine(
    format.colorize({
        all:true
    }),
    format.label({
        label:'[LOGGER]'
    }),
    format.timestamp({
        format:"YY-MM-DD HH:MM:SS"
    }),
    format.printf(
        info => ` ${info.label}  ${info.timestamp}  ${info.level} : ${info.message}`
    )
);

export const getLogger = (loggerName: string) => {
    const mainLogger = {
        format: format.combine(
            // limiter(),
            format.json()
        ),
        defaultMeta: { loggerName },
        transports: [new transports.Console()]
    };

    const throttleLogger = {
        format: format.combine(
            limiter(),
            format.json()
        ),
        defaultMeta: { loggerName },
        // transports: [new transports.Console()]
        transports: [
            new (transports.Console)({
                format: format.combine(format.colorize(), alignColorsAndTime)
            })
        ],
    };

    // loggers.add
    loggers.add(LOGGER_TYPE.MAIN, mainLogger);
    loggers.add(LOGGER_TYPE.THROTTLE, throttleLogger)

    return loggers.get(LOGGER_TYPE.THROTTLE)
    // return loggers.get(LOGGER_TYPE.MAIN)
}


export const getThrottleLogger = (logger: Logger, opts: ThrottleOptions) => {
    return logger.child({ throttle: opts })
}
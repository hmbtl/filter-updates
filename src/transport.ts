import Transport from 'winston-transport';
import { TransformableInfo } from "logform"
import { Logger, transports, loggers } from "winston"


export class ThrottleTransport extends Transport {
    private logger: Logger
    constructor(opts: Transport.TransportStreamOptions = {}) {
        super(opts);

        //
        // Consume any custom options here. e.g.:
        // - Connection information for databases
        // - Authentication information for APIs (e.g. loggly, papertrail,
        //   logentries, etc.).
        //
        this.logger = loggers.get("main");
    }

    log(info: TransformableInfo, callback: () => void) {
        setImmediate(() => {
            this.emit('logged', info);
        });


        // this.log(info, callback)
        this.logger.warn("happened x times in given time frame")

        // Perform the writing to the remote service

        callback();
    }
}
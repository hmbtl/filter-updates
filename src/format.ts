/*
 * Copyright 2021-present Evernote Corporation. All rights reserved.
 */
import winston from 'winston';
import { MESSAGE } from 'triple-beam';
import callsite from 'callsite';

import { get, set } from 'lodash';
import hash from 'object-hash';

let lastLogs = {}


const getLogId = () => {
    const stack: string[] = [];
    // Get the current call stack and convert it to the array
    callsite().forEach((site) => {
        if (!site || typeof site.toString !== 'function') return;
        stack.push(site.toString());
    });

    // Hash the stack to create a unique identifier for the log
    return hash(stack);
}



export const throttle = () => {
    return winston.format((info, opts) => {
        // console.log(Object.keys(lastLogs).length)
        // console.log(info.logger)
        if (!info.throttle) {
            return info
        } else {
            const throttleOpts = info.throttle;
            delete info.throttle
            const logHash = hash({ info });
            const now = Date.now();
            const previousLog = get(lastLogs, logHash, null);

            // Check if there's no previous log, or if the previous log has since timed out
            if (!previousLog) {
                const count = 1;
                // remove last logs
                // lastLogs = {}
                // Add the log meta to the previous logs
                set(lastLogs, logHash, { now, count });

                // Forward the log to the logger
                info.count = count
                return info;
            } else {
                // more than duration is passed then we need to print message
                if (now > previousLog.now + throttleOpts.duration){
                    const count = 1;
                    set(lastLogs, logHash, null);

                    // Forward the log to the logger
                    info.message = `${info.message}: this log message appeared ${previousLog.count - throttleOpts.numberOfLogs} more times in ${throttleOpts.duration} ms`
                    // info.count = count
                    console.log(lastLogs)
                    info.level = 'warn'
                    return info;
                }

                const count = previousLog.count + 1;
                const duration = previousLog.now
                set(lastLogs, logHash, { now: duration, count });

                if (count <= throttleOpts.numberOfLogs) {
                    info.count = count
                    return info
                } 
            }
            return false;
        }

    })
}
import { getLogger, getThrottleLogger, ThrottleOptions } from "./logger"

// get logger instance like in en-node-logger
const logger = getLogger("test:test");

// pass logger instance to create child logger with throttling behaviour
const throttleOpts: ThrottleOptions = {
    numberOfLogs: 10,
    duration: 5000,
}

const throttleOptsSecond: ThrottleOptions = {
    numberOfLogs: 1,
    duration: 10000
}


const loggerWithThrottle = getThrottleLogger(logger, throttleOpts);


while (true) {
    // logger without throttling
    // logger.info("Log message without throttling")

    // // throttling using separate method
    // loggerWithThrottle.info("example message with throttling " );

    // // we can pass throttling opts using the method 
    logger.info("example message with throttling", { throttle: throttleOpts })
    // logger.info("example message with throttling second", { throttle: throttleOptsSecond })
}



















/* QUESTIONS

1. If there is a small change on log messages for example timestamp or
some other key:value pairs should we throttle them together or log messages should be identical including the all key:value pairs.

Current mechanism will not throttle these log messages because there are not identicial

{"message":"example message with throttling","level":"info","loggerName":"test:test","timestamp":124141242}
{"message":"example message with throttling","level":"info","loggerName":"test:test","timestamp":235325325}

-----------------------------------
2. WHAT TO PRINT AFTER LIMIT EXCEEDED ?

{"message":"This log message appeared 147409 more times in 5000 ms","level":"warn","loggerName":"test:test"}

-----------------------------------
3. WHEN TO STOP THROTTLING?

Example - 10 logs per 5000ms

{"message":"example message with throttling","level":"info","loggerName":"test:test","count":1}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":2}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":3}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":4}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":5}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":6}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":7}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":8}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":9}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":10}
{"message":"This log message appeared 147022 more times in 5000 ms","level":"warn","loggerName":"test:test"}

----------------------------------
Example - 10 logs per 5000ms

{"message":"example message with throttling","level":"info","loggerName":"test:test","count":1}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":2}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":3}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":4}

{"message":"another log from other module or some other function","level":"info","loggerName":"test:test"}

// should I RESET the throttling or check log message separately ?
// If not then it would be confusing to add warn message.

{"message":"example message with throttling","level":"info","loggerName":"test:test","count":6}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":7}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":8}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":9}
{asasasfasf}

{"message":"This log message appeared 147022 more times in 5000 ms","level":"warn","loggerName":"test:test"}
{"message":"example message with throttling","level":"info","loggerName":"test:test","count":10}

*/


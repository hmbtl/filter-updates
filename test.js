const winston = require('winston');
const _  = require('lodash')

class LimiterTransport extends winston.Transport {
  constructor(options) {
    super(options)
    this.level = options && options.level || 'info'
    this.lastLogs = []
    this.consoleLogger = winston.createLogger({
      transports: [
        new (winston.transports.Console)(options),
      ]
    });

  }

  log(level, msg, meta, callback) {
    let lcl = meta && meta.lcl || null
    let llim = meta && meta.llim || 0
    if (!lcl || llim === 0) {
      this.consoleLogger[level](msg)
    } else {
      let newLog = {
        lcl: lcl,
        at: Date.now(),
      }

      let lastLogIndex = _.findIndex(this.lastLogs, { lcl: lcl})
      if (lastLogIndex === -1 || newLog.at > this.lastLogs[lastLogIndex].at + llim * 1000) {
        delete meta.lcl
        delete meta.llim
        this.consoleLogger[level](msg)
        if (lastLogIndex === -1) {
          this.lastLogs.push(newLog)
        } else {
          this.lastLogs.splice(lastLogIndex, 1, newLog)
        }
      }
    }

    callback(null, true)
  }
}

winston.transports.LimiterTransport = LimiterTransport;

let log = winston.createLogger({
    defaultMeta: { loggerName:"gjhjhh" },
  transports: [
    new (winston.transports.LimiterTransport)(),
  ],
});

while (true)  {
  log.info('Log this maximum once every 10 seconds', { lcl: 'a log class', llim: 10 });
  log.info('Log this maximum once every 15 seconds', { lcl: 'another log class', llim: 15 });
}